# Snake - The game



## Description
As the title of this project might give away, this is an implementation of the famous Snake game. It was implemented as part of a small 1-day challenge with a friend of mine.

## Installation
The project is set up to run on Java 11 in a MacOS or Windows environment. To change the used Java version simply edit the corresponding line (sourceCompatibility) in the build.gradle file. To run the project on a Linux environment, simply add the missing dependencies in the build.gradle file (You can copy and paste the mac or win compiles and change mac or win to linux).

## Usage
The project can be executed by running the class Launcher located in the launcher package or by running the executable jar file located at the /build/libs folder.

Once you successfully started the project you can:
- Start the game by clicking the PLAY button.
- Move the snake with the WASD keys to collect the randomly generated "apples". Make sure to not collide with the border of the window or with a part of the snake's tail or otherwise the game will be over.
- In the Game Over window click the arrow at the top left corner to get back to the initial scene.

## Roadmap
To make the game more interesting, following additions are planned: 
- Add obstacles that the snake can collide with to make the game more difficult
- Change the speed of the snake over time.

## Project status
As mentioned, this project was part of a challenge and can therefore be considered completed. Even though I do have some changes and additions in mind it is not sure whether or not they will be implemented.
