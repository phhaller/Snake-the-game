package launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import view.GameWindowController;
import view.StartWindowController;

import java.io.IOException;

public class Main extends Application {

  private Stage primaryStage;


  @Override
  public void start(Stage primaryStage) throws Exception {

    this.primaryStage = primaryStage;
    this.primaryStage.setTitle("SNAKE");
    this.primaryStage.setResizable(false);

    createWindow();

  }


  private void createWindow() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      AnchorPane background = loader.load();

      Scene scene = new Scene(background);

      StartWindowController controller = loader.getController();
      controller.setScene(scene);

      primaryStage.setScene(scene);
      primaryStage.show();

    } catch (IOException e) {
      e.printStackTrace();
    }


  }



  public static void main(String[] args) {
    launch(args);
  }

}
