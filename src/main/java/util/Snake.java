package util;

import javafx.scene.layout.AnchorPane;

import java.util.LinkedList;

public class Snake {

  private AnchorPane background;
  private LinkedList<Cell> snakeParts;
  private int size;


  public Snake(int x, int y, int size, AnchorPane background) {

    this.background = background;
    this.size = size;
    snakeParts = new LinkedList<>();
    initializeSnake(x, y);

  }


  private void initializeSnake(int x, int y) {

    Cell head = new Cell(x, y, size, "snake");
    snakeParts.add(head);
    background.getChildren().add(head.getCell());

  }


  public void addPart(String dir) {

    Cell newCell = null;

    switch (dir) {
      case "up":
        newCell = new Cell(snakeParts.getLast().getX(), snakeParts.getLast().getY() + 1, size, "snake");
        break;
      case "left":
        newCell = new Cell(snakeParts.getLast().getX() + 1, snakeParts.getLast().getY(), size, "snake");
        break;
      case "down":
        newCell = new Cell(snakeParts.getLast().getX(), snakeParts.getLast().getY() - 1, size, "snake");
        break;
      case "right":
        newCell = new Cell(snakeParts.getLast().getX() - 1, snakeParts.getLast().getY(), size, "snake");
        break;
    }

    if (newCell != null) {
      snakeParts.addLast(newCell);
      background.getChildren().add(newCell.getCell());
      fadeOutParts();
    }

  }


  private void fadeOutParts() {

    int size = snakeParts.size();
    double fadeVal = .7 / size;

    for (int i = 1; i < snakeParts.size(); i++) {
      snakeParts.get(i).getCell().setOpacity(1 - i * fadeVal);
    }

  }


  public LinkedList<Cell> getSnakeParts() {
    return snakeParts;
  }

}
