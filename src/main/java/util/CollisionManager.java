package util;

import java.util.LinkedList;

public class CollisionManager {

  public static boolean checkBorderCollision(Cell head, int width, int height) {
    boolean collided = false;
    if (head.getX() < 1 || head.getX() > width || head.getY() < 1 || head.getY() > height) {
      collided = true;
    }
    return collided;
  }


  public static boolean checkSnakePartCollision(LinkedList<Cell> parts) {
    boolean collided = false;
    Cell head = parts.getFirst();
    for (Cell c : parts) {
      if (c != head && head.getX() == c.getX() && head.getY() == c.getY()) {
        collided = true;
      }
    }
    return collided;
  }


  public static boolean checkAppleCollision(Cell head, Cell apple) {
    boolean collided = false;
    if (head.getX() == apple.getX() && head.getY() == apple.getY()) {
      collided = true;
    }
    return collided;
  }



}
