package util;

import javafx.scene.shape.Rectangle;

import java.util.Random;

public class Cell {

  private int x;
  private int y;
  private int size;
  private String type;
  private Rectangle cell;


  public Cell(int x, int y, int size, String type) {

    this.x = x;
    this.y = y;
    this.size = size;
    this.type = type;

    createCell();

  }

  private void createCell() {

    cell = new Rectangle(size, size);
    cell.setTranslateX(x * size);
    cell.setTranslateY(y * size);

    switch (type) {
      case "regular":
        cell.getStyleClass().add("cellRegular");
        break;
      case "wall":
        cell.getStyleClass().add("cellWall");
        break;
      case "snake":
        cell.getStyleClass().add("cellSnake");
        break;
      case "apple":
        cell.getStyleClass().add("cellApple");
        break;
    }

  }


  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public String getType() {
    return type;
  }

  public Rectangle getCell() {
    return cell;
  }

}
