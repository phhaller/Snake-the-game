package util;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.scene.effect.Glow;
import javafx.util.Duration;

public class Apple {

  private int x;
  private int y;
  private int size;
  private Cell apple;


  public Apple(int x, int y, int size) {

    this.x = x;
    this.y = y;
    this.size = size;

    createApple();

  }


  private void createApple() {
    apple = new Cell(x, y, size, "apple");

    FadeTransition ft = new FadeTransition(Duration.millis(1000), apple.getCell());
    ft.setFromValue(1);
    ft.setToValue(.6);
    ft.setAutoReverse(true);
    ft.setCycleCount(Animation.INDEFINITE);
    ft.play();

    Glow glow = new Glow(.3);
    apple.getCell().setEffect(glow);

  }


  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public Cell getApple() {
    return apple;
  }

}
