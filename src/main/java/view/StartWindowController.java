package view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class StartWindowController {


  private Scene scene;

  @FXML
  private AnchorPane background;

  @FXML
  private Label playLabel;


  private void initialize() {

    createHandlers();

  }


  private void createHandlers() {

    playLabel.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchScene();
      }
    });

  }


  private void switchScene() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      GameWindowController controller = loader.getController();
      controller.setScene(scene);

      stage.getScene().setRoot(root);
      stage.show();


    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  public void setScene(Scene scene) {
    this.scene = scene;
    initialize();
  }

}
