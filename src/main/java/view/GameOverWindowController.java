package view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class GameOverWindowController {

  private Scene scene;
  private Label backLabel;

  @FXML
  private AnchorPane background;

  @FXML
  private Label collectibleLabel;


  private void initialize() {

    createBackLabel();
    createHandlers();

  }


  private void createBackLabel() {

    backLabel = new Label();
    backLabel.setTranslateX(30);
    backLabel.setTranslateY(30);
    backLabel.setGraphic(new ImageView(new Image("/pictures/backArrow.png")));
    backLabel.getStyleClass().add("backLabel");

    background.getChildren().add(backLabel);

  }


  private void createHandlers() {

    backLabel.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        switchScene();

      }
    });

  }


  private void switchScene() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      StartWindowController controller = loader.getController();
      controller.setScene(scene);

      stage.getScene().setRoot(root);
      stage.show();


    } catch (IOException e) {
      e.printStackTrace();
    }

  }





  public void setScene(Scene scene) {
    this.scene = scene;
    initialize();
  }

  public void setCollected(int collected) {
    collectibleLabel.setText(String.valueOf(collected));
  }

}
