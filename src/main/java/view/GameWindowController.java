package view;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import util.Apple;
import util.Cell;
import util.CollisionManager;
import util.Snake;

import java.io.IOException;
import java.util.Random;

public class GameWindowController {

  private Scene scene;
  private int cellSize = 10;
  private Cell[][] cells;
  private Snake snake;
  private String dir = "right";
  private Timeline timeline;
  private Apple apple;
  private int collectedApples = 0;

  @FXML
  private AnchorPane background;


  private void initialize() {

    createCells();
    createSnake();
    createApple();
    createHandlers();
    createMovement();

  }


  private void createCells() {

    cells = new Cell[1200/cellSize][800/cellSize];

    for (int y = 0; y < cells[0].length; y++) {
      for (int x = 0; x < cells.length; x++) {

        if (x == 0 || x == 1200/cellSize-1 || y == 0 || y == 800/cellSize-1) {
          cells[x][y] = new Cell(x, y, cellSize, "wall");
        } else {
          cells[x][y] = new Cell(x, y, cellSize, "regular");
        }

        background.getChildren().add(cells[x][y].getCell());

      }
    }

  }


  private void createSnake() {

    snake = new Snake(10, 30, cellSize, background);

  }


  private void createApple() {

    Random random = new Random();
    int randX = random.nextInt(119 - 1) + 1;
    int randY = random.nextInt(79 - 1) + 1;

    apple = new Apple(randX, randY, cellSize);

    background.getChildren().add(apple.getApple().getCell());

  }


  private void createHandlers() {

    scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        switch (event.getCode()) {

          case W:
            dir = "up";
            break;
          case A:
            dir = "left";
            break;
          case S:
            dir = "down";
            break;
          case D:
            dir = "right";
            break;
          case P:
            if (timeline.getStatus().equals(Animation.Status.RUNNING)) {
              timeline.pause();
            } else {
              timeline.play();
            }
            break;

        }

      }
    });

  }


  private void createMovement() {

    timeline = new Timeline(new KeyFrame(Duration.millis(40), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (dir.equals("up")) {
          Cell head = new Cell((int) snake.getSnakeParts().getFirst().getX(), (int) snake.getSnakeParts().getFirst().getY(), cellSize, "snake");
          snake.getSnakeParts().getFirst().getCell().setTranslateY(snake.getSnakeParts().getFirst().getCell().getTranslateY() - cellSize);
          snake.getSnakeParts().getFirst().setY(snake.getSnakeParts().getFirst().getY() - 1);
          updateParts(head);
        }

        if (dir.equals("left")) {
          Cell head = new Cell((int) snake.getSnakeParts().getFirst().getX(), (int) snake.getSnakeParts().getFirst().getY(), cellSize, "snake");
          snake.getSnakeParts().getFirst().getCell().setTranslateX(snake.getSnakeParts().getFirst().getCell().getTranslateX() - cellSize);
          snake.getSnakeParts().getFirst().setX(snake.getSnakeParts().getFirst().getX() - 1);
          updateParts(head);
        }

        if (dir.equals("down")) {
          Cell head = new Cell((int) snake.getSnakeParts().getFirst().getX(), (int) snake.getSnakeParts().getFirst().getY(), cellSize, "snake");
          snake.getSnakeParts().getFirst().getCell().setTranslateY(snake.getSnakeParts().getFirst().getCell().getTranslateY() + cellSize);
          snake.getSnakeParts().getFirst().setY(snake.getSnakeParts().getFirst().getY() + 1);
          updateParts(head);
        }

        if (dir.equals("right")) {
          Cell head = new Cell((int) snake.getSnakeParts().getFirst().getX(), (int) snake.getSnakeParts().getFirst().getY(), cellSize, "snake");
          snake.getSnakeParts().getFirst().getCell().setTranslateX(snake.getSnakeParts().getFirst().getCell().getTranslateX() + cellSize);
          snake.getSnakeParts().getFirst().setX(snake.getSnakeParts().getFirst().getX() + 1);
          updateParts(head);
        }

        // You went off map or collided with your tail
        if (CollisionManager.checkBorderCollision(snake.getSnakeParts().getFirst(), 1200/cellSize-2, 800/cellSize-2) ||
            CollisionManager.checkSnakePartCollision(snake.getSnakeParts())) {
          timeline.stop();
          switchScene();
        }

        if (CollisionManager.checkAppleCollision(snake.getSnakeParts().getFirst(), apple.getApple())) {
          background.getChildren().remove(apple.getApple().getCell());
          snake.addPart(dir);
          createApple();
          collectedApples += 1;
        }


      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }


  private void updateParts(Cell head) {

    Cell last = null;
    Cell current = head;

    for (int i = 1; i < snake.getSnakeParts().size(); i++) {

      last = new Cell((int) snake.getSnakeParts().get(i).getX(), (int) snake.getSnakeParts().get(i).getY(), cellSize, "snake");

      snake.getSnakeParts().get(i).getCell().setTranslateX(current.getCell().getTranslateX());
      snake.getSnakeParts().get(i).setX(current.getX());
      snake.getSnakeParts().get(i).getCell().setTranslateY(current.getCell().getTranslateY());
      snake.getSnakeParts().get(i).setY(current.getY());
      current = last;

    }

  }


  private void switchScene() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameOverWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      GameOverWindowController controller = loader.getController();
      controller.setScene(scene);
      controller.setCollected(collectedApples);

      stage.getScene().setRoot(root);
      stage.show();


    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  public void setScene(Scene scene) {
    this.scene = scene;
    initialize();
  }

}
